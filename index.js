// Concepts and Theory Exam:
// https://forms.gle/GapYaHb6LWkNHw1Z6 


// Mock Technical Exam (Function Coding)
// No need to create a test inputs, the program automatically checks any input through its parameter/s
// Use the parametrs as your test input
// Make sure to use return keyword so your answer will be read by the tester (Example: return result;);
// - Do not add or change parameters
// - Do not create another file
// - Do not share this mock exam

// Use npm install and npm test to test your program

function countLetter(letter, sentence) {
    if (letter.length !==1) {
        return undefined;
    }

    let result = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }

    return result;
}

function isIsogram(text) {
    text = text.toLowerCase();
  
    const uniqueChars = new Set();
    
    for (let i = 0; i < text.length; i++) {
        if (uniqueChars.has(text[i])) {
            return false;
        } else {
            uniqueChars.add(text[i]);
        }
    }
  
    return true;
}

function purchase(age, price) {

    if (age<13) {
        return undefined;
    }
    else if (age>=13 && age<= 21 || age >=65) {
        const discountedPrice = price *0.8;
        return discountedPrice.toFixed(2);
    }
    else {
        return price.toFixed(2);
    }
    
}

function findHotCategories(items) {
  const hotCategories = new Set();
  for (const item of items) {
    if (item.stocks === 0) {
      hotCategories.add(item.category);
    }
  }
  return Array.from(hotCategories);
}

function findFlyingVoters(candidateA, candidateB) {
    const setA = new Set(candidateA);
    const flyingVoters = [];

    for (let i = 0; i < candidateB.length; i++) {
        const voter = candidateB[i];
        if (setA.has(voter)) {
            flyingVoters.push(voter);
        }
    }
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};